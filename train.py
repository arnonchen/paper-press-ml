import numpy as np
import tensorflow as tf
import cv2 as cv
import os

x = []
y = []
dir = os.listdir(r'C:\Users\alona\Desktop\DB\not_touching')
for i in dir:
    img = cv.imread(fr'C:\Users\alona\Desktop\DB\not_touching\{i}')
    img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    img = img / 255.0
    img = cv.resize(img, (28,28))
    img = np.reshape(img, (28, 28, 1))
    x.append(img)
    y.append(0)
dir = os.listdir(r'C:\Users\alona\Desktop\DB\pink')
for i in dir:
    img = cv.imread(fr'C:\Users\alona\Desktop\DB\pink\{i}')
    img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    img = img / 255.0
    img = cv.resize(img, (28, 28))
    img = np.reshape(img, (28,28,1))
    x.append(img)
    y.append(1)

x_test = []
y_test = []
dir = os.listdir(r'C:\Users\alona\Desktop\test\not_touching')
for i in dir:
    img = cv.imread(fr'C:\Users\alona\Desktop\test\not_touching\{i}')
    img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    img = img / 255.0
    img = cv.resize(img, (28,28))
    img = np.reshape(img, (28, 28, 1))
    x_test.append(img)
    y_test.append(0)
dir = os.listdir(r'C:\Users\alona\Desktop\test\pink')
for i in dir:
    img = cv.imread(fr'C:\Users\alona\Desktop\test\pink\{i}')
    img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    img = img / 255.0
    img = cv.resize(img, (28, 28))
    img = np.reshape(img, (28,28,1))
    x_test.append(img)
    y_test.append(1)


x = np.asarray(x)
y = np.asarray(y)
x_test = np.asarray(x_test)
y_test = np.asarray(y_test)

model = tf.keras.models.Sequential([
    tf.keras.layers.Reshape((28,28,1), input_dim=28*28),
    tf.keras.layers.Conv2D(60,(3,3), activation='relu'),
    tf.keras.layers.MaxPooling2D(),
    tf.keras.layers.Conv2D(30,(3,3), activation='relu'),
    tf.keras.layers.MaxPooling2D(),
    tf.keras.layers.Flatten(),
    tf.keras.layers.Dense(64,activation='relu'),
    tf.keras.layers.Dropout(0.4),
    tf.keras.layers.Dense(32,activation='relu'),
    tf.keras.layers.Dropout(0.4),
    tf.keras.layers.Dense(2, activation=tf.nn.softmax)
])

model._name = 'pink_or_nothing'

# choose optimiser
optimizer = tf.keras.optimizers.SGD(learning_rate=0.001, momentum=0.9)

# compile model
model.compile(optimizer=optimizer, loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=False), metrics=['accuracy'])

print("Train")
model.fit(x[:],y[:],epochs=40)
print('evaluate')
loss, acc = model.evaluate(x_test,y_test)
model.save(r'C:\Users\alona\Desktop\model')
