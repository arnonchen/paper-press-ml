import cv2
import numpy as np
import tensorflow as tf
import time
import playsound

flag = True
video_capture = cv2.VideoCapture(0)
model_path = r'C:\Users\alona\Desktop\model'
options = [0, 1]
model = tf.keras.models.load_model(model_path)
running = True
option = 0
while running:
    ret, frame = video_capture.read()
    img = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    img = img / 255.0
    img = cv2.resize(img, (28, 28))
    img = np.reshape(img, (28, 28, 1))
    prediction = model.predict(np.array([img]))
    prediction = options[np.argmax(prediction)]
    print(prediction)
    if prediction == 1 and flag:
        playsound.playsound(r"C:\Users\alona\Desktop\kick.mp3")
        flag = False
    if prediction == 0 and not flag:
        flag = True
    cv2.imshow('gerbil', frame)
    cv2.waitKey(1)
    time.sleep(0.01)
